import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  genders = ['male', 'female'];
  forbiddenUsernames = ['shiq', 'crap'];

  constructor() { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      'userData': new FormGroup({
        // 'username': new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]),
        'username': new FormControl(null, [Validators.required, this.forbiddenNames]),
        'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails),
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([])
    });

    this.registerForm.get('gender').valueChanges.subscribe(val=> {
      let name = val === 'male' ? 'Ivan' : 'Ivanka'
      // this.registerForm.patchValue({
      //   'userData': {
      //     'username': name
      //   }
      // });
      
    });
  
  }

  getControls() {
    return (this.registerForm.get('hobbies') as FormArray).controls;
  }
  onAddHobby() {
    const control = new FormControl(null, Validators.required);
    ( <FormArray> this.registerForm.get('hobbies') ).push(control);
  }

  // forbiddenNames(control: FormControl): { [s: string]: boolean } {
  //   if (this.forbiddenUsernames.indexOf(control.value) !== -1) {
  //     return {'nameIsForbidden': true};
  //   }
  //   return null;
  // }  

  forbiddenNames = (control: FormControl): { [s: string]: boolean } => {
    if (this.forbiddenUsernames.indexOf(control.value) !== -1) {
      return {'nameIsForbidden': true};
    }
    return null;
  }

  forbiddenEmails(control: FormControl): Promise<any> | Observable<any> {

    const promise = new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        if (control.value === 'test@test.com') {
          resolve({'emailIsForbidden': true});
        } else {
          resolve(null);
        }
      }, 2000);
    });

    return promise;
  }

  onSubmit() {
    console.log(this.registerForm);
    // this.registerForm.reset({'gender': 'male'});
  }
  onClear() {
    this.registerForm.reset({'gender': 'male'});
  }
}
