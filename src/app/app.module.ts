import { AuthInterceptingService } from './basic-http/auth-intercepting.service';
import { LoggingInterceptorService } from './basic-http/logging-interceptor.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { RegisterComponent } from './register/register.component';
import { ServersComponent } from './servers/servers.component';
import { ShortenPipe } from './shorten.pipe';
import { FilterPipe } from './filter.pipe';
import { HttpClientModule,  HTTP_INTERCEPTORS } from '@angular/common/http';
import { BasicHttpComponent } from './basic-http/basic-http.component';
import { AlertComponent } from './shared/alert/alert.component';

@NgModule({
  // declare components, directives, pipes
  declarations: [
    AppComponent,
    SignupComponent,
    RegisterComponent,
    ServersComponent,
    ShortenPipe,
    FilterPipe,
    BasicHttpComponent,
    AlertComponent,
  ],
  // Imports makes the exported declarations of other modules available
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  // Providers are used to make services and values known to dependency injection
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoggingInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptingService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
