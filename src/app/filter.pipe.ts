import { R3TargetBinder } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(target: any, filterString: string, propName: string): any {

    if (!target || !filterString) {
        return target;
    }
    
    return target.filter( (el) => el.status === filterString)
  }

}