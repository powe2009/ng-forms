import { Component, ViewChild, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

// In Angular, a Directive is essentially a typescript class which has
// been annotated with a TypeScript Decorator. The decorator is the @ symbol.
//
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent  {
  // If we want to access a child component, directive, DOM element inside 
  // the parent component, we use the decorator @ViewChild()
  //
  @ViewChild('f') signupForm: NgForm
 
  defaultSecret = 'pet';
  answer = '';
  genders = ['male', 'female'];
  user = {
    username: '',
    email: '',
    secretQuestion: '',
    answer: '',
    gender: ''
  };

  message:string;
  submitted = false;

  suggestUserName(e) {
    const suggestedName = 'Superuser';
    // this.signupForm.setValue({
    //   username: suggestedName,
    //   email: '',
    //   secret: '',
    //   questionAnswer: '',
    //   gender: ''
    // })
    this.signupForm.form.patchValue({
      username: suggestedName,
      email: 'abc@abc.net',
      secret: 'pet'
    })
  }

  onClose($event) {
    this.submitted = false;
  }

  onSubmit(form: NgForm) {
    // form.controls.secret.setValue(this.defaultSecret);
    this.submitted = true;
    this.user.username = this.signupForm.value.username;
    this.user.email = this.signupForm.value.email;
    this.user.secretQuestion = this.signupForm.value.secret;
    this.user.answer = this.signupForm.value.questionAnswer;
    this.user.gender = this.signupForm.value.gender;

    this.message = this.user.username + " is signed up";
   
    this.signupForm.reset();
  }
}
