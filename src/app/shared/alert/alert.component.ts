import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent {
  // accept value from the outside
  @Input() message: string;

  // allows to close this component from the outside: "close me!"
  @Output() close = new EventEmitter<void>();

  onClose(ev) {
    // console.log('close ev', ev)   // MouseEvent

    // close the emitted event, but not removes the component
    this.close.emit();
  }

}
