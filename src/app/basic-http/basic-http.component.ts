/* tslint:disable:no-trailing-whitespace */
import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Post } from './post.model';
import { PostsService } from './posts.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'basic-http',
  templateUrl: './basic-http.component.html',
  styleUrls: ['./basic-http.component.css']
})
export class  BasicHttpComponent implements OnInit {
  loadedPosts: Post[] = [];
  isFetching = false;
  error = null;

  constructor(private http: HttpClient, private postsService: PostsService) {}

  // tslint:disable-next-line:typedef
  ngOnInit() {
      this.postsService.fetchPosts().subscribe(
        posts => {
          this.isFetching = false;
          this.loadedPosts = posts;
        },
        error => {
          this.isFetching = false;
          // this.error = error.message;
          this.error = JSON.stringify(error.error);
          console.log(error);
        }
    );
  }

  onCreatePost(postForm: NgForm): void {
      const postData: { title: string, content: string } = postForm.value;
      this.postsService.createPost(postData.title, postData.content);
      // this.onFetchPosts();   doesn't work here
      this.onClearForm(postForm);
  }

  onDeleteAllPosts(): void {
    this.postsService.deleteAllPosts()
      .subscribe ( () => {
          this.loadedPosts = [];
      });
  }

  onFetchPosts(): void {
    this.isFetching = true;
    this.postsService.fetchPosts().subscribe(
      posts => {
        this.isFetching = false;
        this.loadedPosts = posts;
      },
      error => {
        this.error = error.message;
        console.log(error);
      }
    );
  }

  onClearError(): void {
    this.error = null;
  }
  onClearForm(f: NgForm): void {
    f.resetForm();
  }

}
