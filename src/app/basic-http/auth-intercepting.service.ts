import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler
} from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptingService implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const modifiedRequest = req.clone({
      headers: req.headers.append('Auth', 'xyz')
    });

    // return next.handle(modifiedRequest);
    return next.handle(modifiedRequest).pipe(tap(
      event => {
        console.log('event', event)
      }
    ))
  }
}


