import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpEventType
} from '@angular/common/http';
import { map,  tap, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Subject, throwError } from 'rxjs';

import { Post } from './post.model';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  error = new Subject<string>();

  constructor(private http: HttpClient) {}

  fetchPosts(): Observable<Post[]> {
    let searchParams = new HttpParams();
    searchParams = searchParams.append('print', 'pretty');
    searchParams = searchParams.append('custom', 'key');

    return this.http
      .get <{ [key: string]: Post }> (
        'https://ng-forms-c24d9.firebaseio.com/posts.json',
        {
          headers: new HttpHeaders({ 'Custom-Header': 'Hello' }),
          params: searchParams,
          responseType: 'json'
        }
      )
      .pipe(
          map(responseData => {
              const postsArray: Post[] = [];
              for (const key in responseData) {
                if (responseData.hasOwnProperty(key)) {
                    postsArray.push({ ...responseData[key], id: key });
                }
              }
              return postsArray;
          }),
          catchError(errorRes => {
              return throwError(errorRes);
          })
      );
  }

  createPost(title: string, content: string): any {
    const postData: Post = { title, content};
    this.http
      .post <{ name: string }> (
          'https://ng-forms-c24d9.firebaseio.com/posts.json',
          postData,
          {
            observe: 'response'
          }
      )
      .subscribe(
          responseData => {
              console.log('post.service.subscribe.responseData: ', responseData);
          },
          error => {
            this.error.next(error.message);
            console.log('createPost.error', error);
          }
      );
  }

  // deleteAllPosts(): Observable<any> {
  //   return this.http.delete (
  //     'https://ng-forms-c24d9.firebaseio.com/posts.json',
  //   );
  // }
  deleteAllPosts(): Observable<any> {
    return this.http.delete (
      'https://ng-forms-c24d9.firebaseio.com/posts.json',
      {
        observe: 'events',
        responseType: 'text'
      } )
      .pipe(
        tap(event => {
          console.log('event:', event);
          if (event.type === HttpEventType.Sent) {
            // ...
          }
          if (event.type === HttpEventType.Response) {
            console.log('event.body:', event.body);
          }
        })
      );
  }

}
