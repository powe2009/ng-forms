https://www.udemy.com/course/the-complete-guide-to-angular-2/learn/lecture/14466460#questions

https://console.firebase.google.com/?pli=1
    My Project -> Realtime database -> https://ng-forms-c24d9.firebaseio.com/
    Realtime dB - https://console.firebase.google.com/project/ng-forms-c24d9/database/ng-forms-c24d9/data
    
Observable
    https://www.learnrxjs.io/learn-rxjs/concepts/get-started-transforming
    https://www.w3resource.com/angular/observables.php

Angular
    A module is made from components, directives, services
    main.ts -> AppModule -> AppComponent

    c:\work\ng-forms\src\app\shared>ng g c alert

https://cli.angular.io/
    npm i -g @angular/cli

    ng new ng-forms                   -or- npm i // existing app
    
    cd ng-forms
    ng serve
    http://localhost:4200/

    ng build
        dist/
        prod

    c:\work\ng-forms\src\app>ng g c servers

Bootstrap 
    npm i bootstrap
        The Bootstrap 4 assets will be installed in the node_modules/bootstrap folder
    Add 
        @import "~bootstrap/dist/css/bootstrap.css" to src/styles.css 

Prettier
    COMMAND + SHIFT + P
        Format document

GIT
    https://gitlab.com/powe2009/
    https://github.com/LubaGmail
   
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
    git remote add origin https://gitlab.com/powe2009/ng-forms.git

    rm -r .git        del -r .git 
    git init
    git add .
    git commit -m "node react aws reset pass"
    git push --set-upstream https://gitlab.com/powe2009/ng-forms.git master
    
    git-create-a-new-branch
    git checkout -b <branch-name>
    git push --set-upstream https://gitlab.com/powe2009/ng-forms.git branch-name

     
TEST
    https://karma-runner.github.io
        ng test
    http://www.protractortest.org
        ng e2e

Auth
----
Encoding is for maintaining data usability and can be reversed by employing the same algorithm that encoded the content, i.e. no key is used. 
Encryption is for maintaining data confidentiality and requires the use of a key (kept secret) in order to return to plaintext.
Private key is generated and kept on the server.

https://firebase.google.com/docs/reference/rest/auth#section-create-email-password
    Web API Key: AIzaSyD0ePNQnZk_xZryT3nhpmcEDwIWJictG0Y

    signup:  Use Git Bash - Ctrl-Shift-Ins to paste text
        curl 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyD0ePNQnZk_xZryT3nhpmcEDwIWJictG0Y' \
        -H 'Content-Type: application/json' \
        --data-binary '{"email":"abc@example.com","password":"abc123","returnSecureToken":true}'



